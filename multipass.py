#!/usr/bin/env python3

from commands import install, uninstall, add_profile, del_profile
from core import setup_parser


def main():
    parser = setup_parser()
    args = parser.parse_args()

    if args.cmd == 'install':
        print('installing...')
        install.run(args.gpgid, args.profile, args.repository, args.install_qtpass, args.mode)

    elif args.cmd == 'uninstall':
        print('uninstalling...')
        uninstall.run(qtonly=args.uninstall_qtpass_only)

    elif args.cmd == 'add_profile':
        print('adding profile...')
        add_profile.run(args.gpgid, args.profile, args.repository, args.mode)

    elif args.cmd == 'del_profile':
        print('deleting profile...')
        del_profile.run(args.profile)

    elif args.cmd == 'execute':
        raise NotImplemented

    else:
        print(parser.print_help())


if __name__ == '__main__':
    main()
    # try:
    #     main()
    # except Exception as e:
    #     print()
    #     print('Error: ' + str(e))
