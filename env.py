#!/usr/bin/env python3
import os
import platform
import stat
from abc import abstractmethod, ABC
from os.path import expanduser
from shutil import rmtree

from config import FileConfig, QtRegConfig


class BaseEnv(ABC):
    qtpass_conf_template_main_section = "General"
    qtpass_conf_template = {
        "addGPGId": "true",
        "autoPull": "false",
        "autoPush": "true",
        "passTemplate": 'login\\nurl',
        "useTemplate": 'true',
        "useClipboard": "true",
        "useGit": "true",
        "usePwgen": "true",
        "usePass": "false",
        "hidePassword": "true",
        "passwordChars": "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%%^&*()_-+={}[]|:;<>,.?"
    }

    @staticmethod
    def create():
        """
        memo: https://github.com/hpcugent/easybuild/wiki/OS_flavor_name_version
        """
        # FIXME: upgrade dist to something else

        if "Linux" in platform.system():
            if "debian" in platform.dist()[0] or "Ubuntu" in platform.dist()[0]:
                return DebianEnv()

            elif "arch" in platform.dist()[0]:
                return ArchEnv()

            else:
                raise NotImplemented('Environment not defined for current OS')

        elif "Darwin" in platform.system():
            raise NotImplemented('Environment not defined for current OS')

        elif "Windows" in platform.system():
            return WindowsEnv()

    def __init__(self):
        self._qt_config = None
        self._multipass_config = None

    @abstractmethod
    def get_qtpass_config(self):
        raise NotImplemented

    @abstractmethod
    def get_multipass_config(self):
        raise NotImplemented

    @abstractmethod
    def build_path(self, *args):
        raise NotImplemented

    @abstractmethod
    def is_installed(self, package_name):
        raise NotImplemented

    @abstractmethod
    def get_executable_path(self, executable):
        raise NotImplemented

    @abstractmethod
    def get_package_name(self, package):
        raise NotImplemented

    @abstractmethod
    def get_path_separator(self):
        raise NotImplemented

    @staticmethod
    @abstractmethod
    def get_git_format_path(string):
        raise NotImplemented

    def init_qtpass_config(self):
        self._qt_config = self.get_qtpass_config()
        for key, value in self.qtpass_conf_template.items():
            self._qt_config.set(self.qtpass_conf_template_main_section, key, value)

        self._qt_config.set(self.qtpass_conf_template_main_section, 'gitExecutable', self.get_executable_path('git'))
        self._qt_config.set(self.qtpass_conf_template_main_section, 'gpgExecutable', self.get_executable_path('gnupg2'))
        self._qt_config.set(self.qtpass_conf_template_main_section, 'passExecutable', self.get_executable_path('pass'))
        self._qt_config.set(self.qtpass_conf_template_main_section, 'pwgenExecutable',
                            self.get_executable_path('pwgen'))
        self._qt_config.save()

    @abstractmethod
    def uninstall(self):
        raise NotImplemented


class LinuxEnv(BaseEnv, ABC):

    multipass_base_path = expanduser("~") + '/' + '.multipass-store'
    multipass_config_path = multipass_base_path + '/' + '.config'

    @staticmethod
    def get_git_format_path(string):
        return string

    def get_path_separator(self):
        return "/"

    def build_path(self, *args):
        parts = list(args)

        if len(parts) < 1:
            raise Exception('al least 1 parameter is required')

        if not all(isinstance(item, str) for item in parts):
            raise Exception('all items must be strings')

        # normalise parts, remove first separator if present
        if parts[0][0] == self.get_path_separator():
            parts[0] = parts[0][1:]

        return self.get_path_separator() + self.get_path_separator().join(parts)

    def uninstall(self):
        rmtree(self.multipass_base_path)

    def get_qtpass_config(self):
        if self._qt_config is None:
            self._qt_config = FileConfig(self.build_path(expanduser("~"), '.config/IJHack', 'QtPass.conf'))
        return self._qt_config

    def get_multipass_config(self):
        if self._multipass_config is None:
            self._multipass_config = FileConfig(self.multipass_config_path)
        return self._multipass_config

    def get_package_name(self, package):
        return self.package_map[package]

    def get_executable_path(self, executable):
        return self.executable_map[executable]


class DebianEnv(LinuxEnv):

    package_map = {
        'gnupg2': 'gnupg2',
        'pwgen': 'pwgen',
        'git': 'git',
        'pass': 'pass',
        'qtpass': 'qtpass'
    }
    executable_map = {
        'gnupg2': '/usr/bin/gpg2',
        'pwgen': '/usr/bin/pwgen',
        'git': '/usr/bin/git',
        'pass': '/usr/bin/pass'
    }

    def is_installed(self, package_name):
        import apt
        cache = apt.Cache()
        cache.open()
        return cache[self.package_map[package_name]].is_installed


class ArchEnv(LinuxEnv):

    package_map = {
        'gnupg2': 'gnupg',
        'pwgen': 'pwgen',
        'git': 'git',
        'pass': 'pass',
        'qtpass': 'qtpass'
    }
    executable_map = {
        'gnupg2': '/usr/bin/gpg2',
        'pwgen': '/usr/bin/pwgen',
        'git': '/usr/bin/git',
        'pass': '/usr/bin/pass'
    }

    def is_installed(self, package_name):
        import pacman
        return pacman.is_installed(self.package_map[package_name])


class WindowsEnv(BaseEnv):
    _executable_map = {
        'gnupg2': 'pub\\gpg2.exe',
        'pwgen': 'PWGen.exe',
        'git': 'bin\\git.exe',
        'pass': 'Pass4Win.exe',
        'qtpass': 'qtpass.exe'
    }

    multipass_base_path = expanduser("~") + '\\' + '.multipass-store'
    multipass_config_path = multipass_base_path + '\\' + '.config'

    # this Fix Windows non-posix behaviour for deleting read-only file
    @staticmethod
    def remove_readonly(func, path, _):
        os.chmod(path, stat.S_IWRITE)
        func(path)

    @staticmethod
    def get_git_format_path(string):
        return string.replace('\\', '/')

    def __init__(self):
        super().__init__()
        self._app_data_dir = self.build_path(expanduser("~"), 'AppData', 'local', 'multipass')
        self.package_map = {

            'gnupg2': {
                'name': 'Gpg4win',
                'path': '',
                'installed': False
            },
            'pwgen': {
                'name': 'PWGen',
                'path': '',
                'installed': False
            },
            'git': {
                'name': 'Git',
                'path': '',
                'installed': False,
            },
            'pass': {
                'name': 'Pass4Win',
                'path': '',
                'installed': False
            },
            'qtpass': {
                'name': 'QtPass',
                'path': '',
                'installed': False
            }
        }
        self._configure()

    def build_path(self, *args):
        # remove last slash per item
        parts = [item[:-1] if item[-1] == '\\' else item for item in args]
        if len(parts) < 1:
            raise Exception('al least 1 parameter is required')

        if not all(isinstance(item, str) for item in parts):
            raise Exception('all items must be strings')

        return self.get_path_separator().join(parts)

    def get_qtpass_config(self):
        if self._qt_config is None:
            self._qt_config = QtRegConfig()
        return self._qt_config

    def get_multipass_config(self):
        if self._multipass_config is None:
            self._multipass_config = FileConfig(self.multipass_config_path)
        return self._multipass_config

    def get_path_separator(self):
        return "\\"

    def get_package_name(self, package):
        return self.package_map[package]['name']

    def get_executable_path(self, executable):
        return self.package_map[executable]['path']

    def is_installed(self, package_name):
        return self.package_map[package_name]['installed']

    def _configure(self):
        import winreg
        package_names = self.package_map.keys()
        software_key_name = r'SOFTWARE'
        s_key = winreg.OpenKeyEx(winreg.HKEY_LOCAL_MACHINE, software_key_name,
                                 access=winreg.KEY_READ | winreg.KEY_WOW64_64KEY)
        win_64_uninstall = r'Microsoft\Windows\CurrentVersion\Uninstall'
        win_32_uninstall = r'WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall'
        u_key_32 = winreg.OpenKeyEx(s_key, win_32_uninstall)
        u_key_64 = winreg.OpenKeyEx(s_key, win_64_uninstall)
        for ukey in (u_key_32, u_key_64):
            for i in range(winreg.QueryInfoKey(ukey)[0]):
                prog_key_name = winreg.EnumKey(ukey, i)
                prog_key = winreg.OpenKeyEx(ukey, prog_key_name)
                for p_name in package_names:
                    try:
                        # print(winreg.QueryValueEx(prog_key, 'DisplayName'))
                        if self.get_package_name(p_name) in winreg.QueryValueEx(prog_key, 'DisplayName')[0]:
                            self.package_map[p_name]['path'] = self.build_path(
                                winreg.QueryValueEx(prog_key, 'InstallLocation')[0].replace('"', ''),
                                self._executable_map[p_name])
                            self.package_map[p_name]['installed'] = True
                    except OSError as e:
                        # print(e)
                        pass
        return False

    def init_qtpass_config(self):
        self._qt_config = self.get_qtpass_config()
        for key, value in self.qtpass_conf_template.items():
            self._qt_config.set(self.qtpass_conf_template_main_section, key, value)

        git_executable = self._windows_ssh_agent_fix()
        self._qt_config.set(self.qtpass_conf_template_main_section, 'gitExecutable', git_executable)
        self._qt_config.set(self.qtpass_conf_template_main_section, 'gpgExecutable', self.get_executable_path('gnupg2'))
        self._qt_config.set(self.qtpass_conf_template_main_section, 'passExecutable', self.get_executable_path('pass'))
        self._qt_config.set(self.qtpass_conf_template_main_section, 'pwgenExecutable',
                            self.get_executable_path('pwgen'))
        self._qt_config.save()

    def _windows_ssh_agent_fix(self):
        try:
            os.mkdir(self._app_data_dir)
            # FIXME: se gia esiste skippa
        except:
            pass

        git_executable = self.get_executable_path('git')

        start_ssh_agent_location = git_executable.replace(self._executable_map['git'], '')
        start_ssh_agent_location = self.build_path(start_ssh_agent_location, 'cmd\start-ssh-agent.cmd')

        # crea file in AppData/local/multipass: che lancia il comando C:\Program Files\Git\cmd\start-ssh-agent.cmd
        if os.path.exists(start_ssh_agent_location):
            with open(self.build_path(self._app_data_dir, 'attach-ssh-agent.cmd'), 'w') as f:
                f.write('"' + start_ssh_agent_location + '"')

        else:
            raise Exception('Git fix fail expecting "start-ssh-agent.cmd" in the location: {}'.format(''))

        # add to reg the autorun of cmd on shell open
        import winreg
        k = winreg.OpenKeyEx(winreg.HKEY_CURRENT_USER, r'SOFTWARE\Microsoft\Command Processor',
                             access=winreg.KEY_ALL_ACCESS)
        winreg.SetValueEx(k, 'AutoRun', 0, winreg.REG_EXPAND_SZ,
                          r'%USERPROFILE%\AppData\local\multipass\attach-ssh-agent.cmd')

        # script lines:
        base_line = 'call "{}" %*\n' \
                    '@ECHO OFF\n'.format(git_executable)

        git_cmd_location = self.build_path(self._app_data_dir, 'git.cmd')
        smart_git_location = self.build_path(self._app_data_dir, 'smart-git.cmd')
        # create new git cmd (used in qtpass)
        with open(git_cmd_location, 'w') as f:
            launching_line = 'if %errorlevel%==1 (START "git %*" /WAIT cmd /c "{} %*") else exit'.format(
                smart_git_location)
            f.write(base_line + launching_line)

        # create smart-git.bat
        with open(smart_git_location, 'w') as f:
            timeout_line = 'timeout 5'
            f.write(base_line + timeout_line)

        return git_cmd_location

    def uninstall(self):
        rmtree(self.multipass_base_path, onerror=self.remove_readonly)
        rmtree(self._app_data_dir, onerror=self.remove_readonly)

        import winreg
        k = winreg.OpenKeyEx(winreg.HKEY_CURRENT_USER, r'SOFTWARE\Microsoft\Command Processor',
                             access=winreg.KEY_ALL_ACCESS)
        winreg.DeleteValue(k, 'AutoRun')
