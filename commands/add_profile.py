#!/usr/bin/env python3

from git import Repo

from core import require_installation, verify_gpgid, sysenv


@require_installation
@verify_gpgid
def run(gpgid, profile, repository, mode):
    # add profile as default in multipass config file
    config = sysenv.get_multipass_config()
    config.load()
    if config.has_key('profiles', profile):
        raise Exception('profile already exists')
    profile_path = sysenv.build_path(sysenv.multipass_base_path, profile)
    config.set('profiles', profile, profile_path)
    config.set('multipass', 'active_profile', profile)
    config.save()

    # setup the repository
    if mode == "create":
        repo = Repo.init(path=profile_path, mkdir=True)
        # defining remote
        origin = repo.create_remote('origin', repository)
        # creating base file and adding to index
        gpgid_path = _create_gpgid(path=profile_path, id=gpgid)
        gitattr_path = _create_gitattributes(path=profile_path)
        gitignore_path = _create_gitignore(path=profile_path)
        repo.index.add([gpgid_path, gitattr_path, gitignore_path])
        repo.index.commit('Create multipass store')
        _add_branch_section_to_gitconfig(repository=repo)
        origin.push()
    elif mode == "import":
        repo = Repo.clone_from(url=repository, to_path=profile_path)

    _add_diff_section_to_gitconfig(repository=repo)

    # if enabled, add profile into qt config file
    if config.get('multipass', 'qt'):
        qt_config = sysenv.get_qtpass_config()
        qt_config.load()
        qt_config.set('profiles', profile, profile_path)
        qt_config.set('General', 'passStore', profile_path)
        qt_config.set('General', 'profile', profile)
        qt_config.save()


def _create_gitattributes(path):
    with open(sysenv.build_path(path, ".gitattributes"), "w") as f:
        f.write("*.gpg diff=gpg")
    return sysenv.build_path(path, ".gitattributes")


def _create_gpgid(path, id):
    with open(sysenv.build_path(path, ".gpg-id"), "w") as f:
        f.write(id)
    return sysenv.build_path(path, ".gpg-id")


def _create_gitignore(path):
    with open(sysenv.build_path(path, ".gitignore"), "w") as f:
        f.write("")
    return sysenv.build_path(path, ".gitignore")


def _add_diff_section_to_gitconfig(repository):
    with repository.config_writer() as writer:
        if not writer.has_section('diff "gpg"'):
            writer.add_section('diff "gpg"')
            writer.set('diff "gpg"', "binary", "true")
            writer.set('diff "gpg"', "textconv", sysenv.get_git_format_path(sysenv.get_executable_path(
                'gnupg2')) + " -d --quiet --yes --compress-algo=none --no-encrypt-to --batch --use-agent")


def _add_branch_section_to_gitconfig(repository):
    with repository.config_writer() as writer:
        if not writer.has_section('branch "master"'):
            writer.add_section('branch "master"')
            writer.set('branch "master"', "remote", "origin")
            writer.set('branch "master"', "merge", "refs/heads/master")
