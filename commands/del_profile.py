#!/usr/bin/env python3
from shutil import rmtree

from core import require_installation, sysenv
from env import WindowsEnv


@require_installation
def run(profile):
    # remove profile from multipass config
    config = sysenv.get_multipass_config()
    config.load()
    if not config.has_key('profiles', profile):
        raise Exception('profile not found')
    if config.count('profiles') < 2:
        raise Exception('you must keep at least 1 profile active')
    config.remove_key('profiles', profile)
    active_profile = config.get('multipass', 'active_profile')
    if active_profile == profile:
        active_profile = config.get_keys('profiles')[0]
        config.set('multipass', 'active_profile', active_profile)
    config.save()

    # delete profile from multipass workspace
    # this Fix Windows non-posix behaviour for deleting read-only file
    rmtree(sysenv.build_path(sysenv.multipass_base_path, profile), onerror=WindowsEnv.remove_readonly)

    # if enabled, remove profile from qtpass config
    if config.get('multipass', 'qt'):
        qt_config = sysenv.get_qtpass_config()
        qt_config.load()
        qt_config.remove_key('profiles', profile)
        active_profile_path = sysenv.build_path(sysenv.multipass_base_path, active_profile)
        qt_config.set('General', 'passStore', active_profile_path)
        qt_config.set('General', 'profile', active_profile)
        qt_config.save()
