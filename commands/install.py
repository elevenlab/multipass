#!/usr/bin/env python3

import os

from core import verify_gpgid, sysenv
from . import add_profile


@verify_gpgid
def run(gpgid, profile, repository, qt, mode):
    """
    Crea la directory base di multipass, con all'interno:
    - un file di configurazione che contiene il nome del profilo default
    - una directory contenente il profilo dell'utente
    """

    # first, check if dependencies are satisfied
    dependencies = ['gnupg2', 'git', 'pass', 'qtpass']
    if qt:
        dependencies.extend(['qtpass', 'pwgen'])
    for package in dependencies:
        if not sysenv.is_installed(package):
            raise Exception(package + ' package missing')

    if os.path.isdir(sysenv.multipass_base_path):
        raise Exception('previous installation exists, use "uninstall" first')

    # create base directory
    os.mkdir(sysenv.multipass_base_path)

    # create the configuration file with active profile name, if
    config = sysenv.get_multipass_config()
    config.set('multipass', 'qt', 'true' if qt else 'false')
    config.save()

    # if qt is enabled, create the config file
    if qt:
        sysenv.init_qtpass_config()

    # create the main profile
    add_profile.run(gpgid=gpgid, profile=profile, repository=repository, mode=mode)
