#!/usr/bin/env python3

from core import require_installation, sysenv


@require_installation
def run(qtonly):
    # if we managed qt configuration file, remove it
    config = sysenv.get_multipass_config()
    config.load()

    if config.get('multipass', 'qt'):
        qt_config = sysenv.get_qtpass_config()
        qt_config.delete()

    if qtonly:
        return

    # remove multipass workspace
    sysenv.uninstall()
