#!/usr/bin/env python3

import argparse
import os
from functools import wraps

from env import BaseEnv

sysenv = BaseEnv.create()


def require_installation(f):
    @wraps(f)
    def req_inst(*args, **kwargs):

        # check for multipass installation path
        if not os.path.exists(sysenv.multipass_base_path):
            raise Exception('multipass is not installed')

        # check for multipass installation config
        if not os.path.exists(sysenv.multipass_config_path):
            raise Exception('multipass config file not found, corruption?')

        return f(*args, **kwargs)

    return req_inst


def verify_gpgid(f):
    @wraps(f)
    def verify(*args, **kwargs):
        # TODO: check for PGP key existence
        return f(*args, **kwargs)

    return verify


def setup_parser():
    parser = argparse.ArgumentParser(description='Utility for pass multi-profile management')
    subparsers = parser.add_subparsers(help='choose one of the options', dest='cmd')

    # create the parser for the "setup" command
    parser_install = subparsers.add_parser('install', help='install multipass')
    parser_install.add_argument('-nqt', '--no-qtpass', dest='install_qtpass', action='store_false',
                                help='do not setup qtpass')
    parser_install.add_argument('-id', '--gpg-id', dest='gpgid', action='store', help='GPG key ID (es. "Name Surname")',
                                required=True)
    parser_install.add_argument('-p', '--profile', dest='profile', action='store', help='profile name (es. personal)',
                                required=True)
    parser_install.add_argument('-r', '--repository', dest='repository', action='store',
                                help='set repository url (es. git@bitbucket.org:user/repo.git)', required=True)
    parser_install.add_argument('-m', '--mode', dest='mode', choices=['create', 'import'])
    parser_install.set_defaults(install_qtpass=True, mode='create')

    # create the parser for the "uninstall" command
    parser_uninstall = subparsers.add_parser('uninstall', help='uninstall')
    parser_uninstall.add_argument('-qto', '--qtpass-only', dest='uninstall_qtpass_only', action='store_true',
                                  help='uninstall qtpass only')
    parser_uninstall.set_defaults(uninstall_qtpass_only=False)

    # create the parser for the "add_profile" command
    parser_add_profile = subparsers.add_parser('add_profile', help='add a profile')
    parser_add_profile.add_argument('-id', '--gpg-id', dest='gpgid', action='store',
                                    help='GPG key ID (es. "Name Surname")', required=True)
    parser_add_profile.add_argument('-p', '--profile', dest='profile', action='store',
                                    help='profile name (es. personal)', required=True)
    parser_add_profile.add_argument('-r', '--repository', dest='repository', action='store',
                                    help='set repository url (es. git@bitbucket.org:user/repo.git)', required=True)
    parser_add_profile.add_argument('-m', '--mode', dest='mode', choices=['create', 'import'],
                                    help='choose create if the repository is empty')
    parser_add_profile.set_defaults(mode='create')

    # create the parser for the "del_profile" command
    parser_del_profile = subparsers.add_parser('del_profile', help='delete a profile')
    parser_del_profile.add_argument('-p', '--profile', dest='profile', action='store',
                                    help='profile name (es. personal)', required=True)

    # create the parser for the "execute" command
    parser_execute = subparsers.add_parser('execute', help='launch shell')

    return parser
