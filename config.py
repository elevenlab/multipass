import os
import sys
from abc import ABC, abstractmethod
from configparser import ConfigParser

if sys.platform == 'win32':
    import winreg


class Config(ABC):
    @abstractmethod
    def load(self):
        raise NotImplemented

    @abstractmethod
    def save(self):
        raise NotImplemented

    @abstractmethod
    def get(self, section, key):
        raise NotImplemented

    @abstractmethod
    def set(self, section, key, value):
        """
        set value for the key in section.
        if section doesn't exist it will created
        """
        raise NotImplemented

    @abstractmethod
    def has_section(self, section):
        raise NotImplemented

    @abstractmethod
    def has_key(self, section, key):
        raise NotImplemented

    @abstractmethod
    def get_keys(self, section):
        raise NotImplemented

    @abstractmethod
    def count(self, section):
        raise NotImplemented

    @abstractmethod
    def remove_key(self, section, key):
        raise NotImplemented

    @abstractmethod
    def delete(self):
        raise NotImplemented


class FileConfig(Config):
    def __init__(self, file, optionxform=str, **kwargs):
        self.config = ConfigParser(**kwargs)
        self.config.optionxform = optionxform
        self.config_file = file

    def set(self, section, key, value):
        if not self.config.has_section(section):
            self.config.add_section(section)
        self.config.set(section, key, value)

    def get(self, section, key):
        if self.config.has_section(section) and self.config.has_option(section, key):
            return self.config.get(section, key)
        else:
            raise Exception('No key {} in section {}'.format(key, section))

    def count(self, section):
        if self.config.has_section(section):
            return len(self.config.options(section))
        else:
            raise Exception('No section {}'.format(section))

    def get_keys(self, section):
        if self.has_section(section):
            return self.config.options(section)
        else:
            raise Exception('No section {}'.format(section))

    def has_section(self, section):
        return self.config.has_section(section)

    def has_key(self, section, key):
        return self.config.has_section(section) and self.config.has_option(section, key)

    def remove_key(self, section, key):
        if self.config.has_option(section, key):
            self.config.remove_option(section, key)
        else:
            raise Exception('No key {} in section {}'.format(key, section))

    def load(self):
        with open(self.config_file) as f:
            self.config.read_file(f)

    def save(self):
        with open(self.config_file, "w") as f:
            self.config.write(f)

    def delete(self):
        if os.path.exists(self.config_file):
            os.remove(self.config_file)


class QtRegConfig(Config):
    def __init__(self):
        self.qt_key_name = 'SOFTWARE\IJHack\QtPass'
        self.qt_key = winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, self.qt_key_name, access=winreg.KEY_ALL_ACCESS)
        self.opened_section = {'General': self.qt_key}

    def set(self, section, key, value):
        if section not in self.opened_section:
            # crea o apre la sezione gia esistente
            self.opened_section[section] = winreg.CreateKeyEx(self.qt_key, section, access=winreg.KEY_ALL_ACCESS)

        section_key = self.opened_section[section]
        # FIXME: type (4th parametro) so cazzi
        winreg.SetValueEx(section_key, key, 0, winreg.REG_SZ, value)

    def get(self, section, key):
        if self.has_section(section):
            section_key = self.opened_section[section]
            try:
                value = winreg.QueryValueEx(section_key, key)[0]
            except OSError:
                raise Exception('No key {} in section {}'.format(key, section))
            return value
        else:
            raise Exception('No section {}'.format(section))

    def count(self, section):
        if self.has_section(section):
            return winreg.QueryInfoKey(self.opened_section[section])[0]
        else:
            raise Exception('No section {}'.format(section))

    def get_keys(self, section):
        if self.has_section(section):
            keys_num = winreg.QueryInfoKey(self.opened_section[section])[1]
            keys = [winreg.EnumValue(self.opened_section[section], i)[0] for i in range(keys_num)]
            return keys
        else:
            raise Exception('No section {}'.format(section))

    def has_section(self, section):
        if section in self.opened_section:
            return True
        try:
            self.opened_section[section] = winreg.OpenKeyEx(self.qt_key, section, access=winreg.KEY_ALL_ACCESS)
        except OSError:
            return False
        else:
            return True

    def has_key(self, section, key):
        if self.has_section(section) is False:
            return False
        section_key = self.opened_section[section]
        try:
            winreg.QueryValueEx(section_key, key)
        except OSError:
            return False
        else:
            return True

    def remove_key(self, section, key):
        if self.has_key(section, key):
            winreg.DeleteValue(self.opened_section[section], key)
        else:
            raise Exception('No key {} in section {}'.format(key, section))

    def load(self):
        pass

    def save(self):
        pass

    def delete(self):
        # open parent key and delte recursively
        parent_key = winreg.OpenKeyEx(winreg.HKEY_CURRENT_USER, 'SOFTWARE', access=winreg.KEY_ALL_ACCESS)

        # delete recursively IJHack sub keys
        key = winreg.OpenKeyEx(parent_key, 'IJHack', access=winreg.KEY_ALL_ACCESS)
        self._recursive_delete(key)

        # delete IJHack key
        winreg.DeleteKey(parent_key, 'IJHack')

    def _recursive_delete(self, key):
        for i in reversed(range(winreg.QueryInfoKey(key)[0])):
            sub_key = winreg.EnumKey(key, i)
            try:
                # print('removing sub_key:', sub_key)
                winreg.DeleteKey(key, sub_key)
            except OSError:
                # print('fail removing: has sub_keys -> accessing', sub_key)
                child_key = winreg.OpenKeyEx(key, sub_key, access=winreg.KEY_ALL_ACCESS)
                self._recursive_delete(child_key)
                winreg.DeleteKey(key, sub_key)
